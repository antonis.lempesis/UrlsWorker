package eu.openaire.urls_worker.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class FilesZipper
{
    private static final Logger logger = LoggerFactory.getLogger(FilesZipper.class);


    public static File zipMultipleFilesAndGetZip(long assignmentsCounter, int zipBatchCounter, List<String> filesToZip, String baseDirectory)
    {
        String zipFilename = baseDirectory + "assignments_" + assignmentsCounter + "_full-texts_" + zipBatchCounter + ".zip";
        // For example: assignments_2_full-texts_4.zip    | where < 4 > is referred to the 4th batch of files requested by the controller.

        int numZippedFiles = 0;
        File zipFile = new File(zipFilename);
        try ( ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile), StandardCharsets.UTF_8) )
        {
            for ( String file : filesToZip ) {
                if ( zipAFile(file, zos, baseDirectory) )
                    numZippedFiles ++;
            }
        } catch (Exception e) {
            logger.error("Exception when creating the zip-file: " + zipFilename, e);
            return null;
        }
        logger.debug("Zipped " + numZippedFiles + " files for assignments_" + assignmentsCounter + ", batch_" + zipBatchCounter);
        return zipFile;
    }


    private static final int BUFFER_SIZE = 3145728;	// 3MB (average fullText-size)
    private static final byte[] dataBuffer = new byte[BUFFER_SIZE];

    // This method is "synchronized" to avoid any future problems with shared-buffer, if the requests are asynchronous.
    private static synchronized boolean zipAFile(String fileName, ZipOutputStream zos, String baseDir)
    {
        boolean shouldCloseEntry = false;   // Useful in order to close the entry in case of an exception.
        String fullFileName = baseDir + fileName;
        try ( BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fullFileName), BUFFER_SIZE) ) {
            zos.putNextEntry(new ZipEntry(fileName));
            shouldCloseEntry = true;
            int count;
            while ( (count = bis.read(dataBuffer, 0, BUFFER_SIZE)) != -1 ) {
                zos.write(dataBuffer, 0, count);
            }
        } catch (FileNotFoundException fnfe) {
            logger.error("Error zipping file: " + fullFileName, fnfe.getMessage());
            return false;
        } catch (Exception e) {
            if ( ! e.getMessage().contains("duplicate") )
                logger.error("Error zipping file: " + fullFileName, e);
            return false;
        } finally {
            if ( shouldCloseEntry ) {
                try {
                    zos.closeEntry();   // close the entry here (not the ZipOutputStream)
                } catch (IOException e) {
                    logger.error("", e);
                }
            }
        }
        return true;
    }

}
