package eu.openaire.urls_worker.controllers;

import eu.openaire.urls_worker.services.FileStorageService;
import eu.openaire.urls_worker.util.FilesZipper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("full-texts/")
public class FullTextsController {

    private static final Logger logger = LoggerFactory.getLogger(GeneralController.class);

    public static HashMap<Long, Boolean> assignmentsNumsHandledAndLocallyDeleted = new HashMap<>();

    public static String assignmentsBaseDir = null;


    public FullTextsController() {
        assignmentsBaseDir = FileStorageService.assignmentsLocation.toString() + File.separator;
    }


    @GetMapping("getFullTexts/{assignmentsCounter:[\\d]+}/{totalZipBatches:[\\d]+}/{zipBatchCounter:[\\d]+}/{fileNamesWithExtensions}")
    public Object getMultipleFullTexts(@PathVariable long assignmentsCounter, @PathVariable int totalZipBatches, @PathVariable int zipBatchCounter, @PathVariable List<String> fileNamesWithExtensions) {

        int fileNamesListNum = fileNamesWithExtensions.size();
        if ( (fileNamesListNum == 1) && (fileNamesWithExtensions.get(0).length() == 0) ) {  // In case the last "/" in the url was given, then this list will not be empty, but have one empty item instead.
            // In case the url does not end in "/", then Spring will automatically return an "HTTP-BadRequest".
            String errorMsg = "An empty \"fileNamesWithExtensions\" list was given from assignments_" + assignmentsCounter + ", for batch_" + zipBatchCounter;
            logger.warn(errorMsg);
            return ResponseEntity.badRequest().body(errorMsg);
        }

        logger.info("Received a \"getMultipleFullTexts\" request for returning a zip-file containing " + fileNamesListNum + " full-texts, from assignments_" + assignmentsCounter + ", for batch_" + zipBatchCounter + " (out of " + totalZipBatches + ").");

        String currentAssignmentsBaseFullTextsPath = assignmentsBaseDir + "assignments_" + assignmentsCounter + "_fullTexts" + File.separator;

        if ( ! (new File(currentAssignmentsBaseFullTextsPath).isDirectory()) ) {
            String errorMsg = "The base directory for assignments_" + assignmentsCounter + " was not found: " + currentAssignmentsBaseFullTextsPath;
            logger.error(errorMsg);
            return ResponseEntity.badRequest().body(errorMsg);
        }

        if ( zipBatchCounter > totalZipBatches ) {
            String errorMsg = "The given \"zipBatchCounter\" (" + zipBatchCounter + ") is greater than the \"totalZipBatches\" (" + totalZipBatches + ")!";
            logger.error(errorMsg);
            return ResponseEntity.badRequest().body(errorMsg);
        }

        File zipFile = FilesZipper.zipMultipleFilesAndGetZip(assignmentsCounter, zipBatchCounter, fileNamesWithExtensions, currentAssignmentsBaseFullTextsPath);
        if ( zipFile == null ) {
            String errorMsg = "Failed to create the zip file for \"zipBatchCounter\"-" + zipBatchCounter;
            logger.error(errorMsg);
            return ResponseEntity.internalServerError().body(errorMsg);
        }

        // If this is the last batch for this assignments-count, then make sure it is deleted in the next scheduled delete-operation.
        if ( zipBatchCounter == totalZipBatches ) {
            assignmentsNumsHandledAndLocallyDeleted.put(assignmentsCounter, false);
            logger.debug("Will return the last batch (" + zipBatchCounter + ") of Assignments_" + assignmentsCounter + " to the Controller and these assignments will be deleted later.");
        }

        String zipName = zipFile.getName();
        String zipFileFullPath = currentAssignmentsBaseFullTextsPath + zipName;
        try {
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + zipName + "\"")
                    .body(new InputStreamResource(new FileInputStream(zipFileFullPath)));
        } catch (Exception e) {
            String errorMsg = "Could not load the FileInputStream of the zip-file \"" + zipFileFullPath + "\"!";
            logger.error(errorMsg, e);
            return ResponseEntity.internalServerError().body(errorMsg);
        }
    }


    @GetMapping("getFullText/{assignmentsCounter:[\\d]+}/{fileNameWithExtension:[\\w_:]+.[\\w]{2,10}}")
    public ResponseEntity<?> getFullText(@PathVariable long assignmentsCounter, @PathVariable String fileNameWithExtension) {

        logger.info("Received a \"getFullText\" request.");
        String fullTextFileFullPath = assignmentsBaseDir + "assignments_" + assignmentsCounter + "_fullTexts" + File.separator + fileNameWithExtension;
        File file = new File(fullTextFileFullPath);
        if ( !file.isFile() ) {
            logger.error("The file \"" + fullTextFileFullPath + "\" does not exist!");
            return ResponseEntity.notFound().build();
        }

        try {
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + file.getName() + "\"")
                    .body(new InputStreamResource(new FileInputStream(fullTextFileFullPath)));
        } catch (Exception e) {
            String errorMsg = "Could not load the FileInputStream of the full-text-file \"" + fullTextFileFullPath + "\"!";
            logger.error(errorMsg, e);
            return ResponseEntity.internalServerError().body(errorMsg);
        }
    }

}
