package eu.openaire.urls_worker.controllers;

import eu.openaire.urls_worker.UrlsWorkerApplication;
import eu.openaire.urls_worker.payloads.responces.WorkerResponse;
import eu.openaire.urls_worker.util.AssignmentsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("")
public class GeneralController {

    private static final Logger logger = LoggerFactory.getLogger(GeneralController.class);


    public GeneralController() {}

    @GetMapping("isAlive")
    public ResponseEntity<?> isWorkerAlive() {

        logger.info("Received an \"isAlive\" request.");

        return ResponseEntity.ok().build();
    }


    @GetMapping("isAvailableForWork")
    public ResponseEntity<?> isWorkerAvailableForWork() {

        logger.info("Received an \"isWorkerAvailableForWork\" request.");

        if ( AssignmentsHandler.isAvailableForWork ) {
            logger.info("The worker is available for an assignment.");
            return ResponseEntity.status(200).body(new WorkerResponse(UrlsWorkerApplication.workerId, UrlsWorkerApplication.maxAssignmentsLimitPerBatch));
        } else {
            logger.info("The worker is busy with another assignment.");
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }


    @GetMapping("getHandledAssignmentsCounts")
    public ResponseEntity<?> getHandledAssignmentsCounts()
    {
        List<Long> handledAssignmentsCounts = new ArrayList<>(FullTextsController.assignmentsNumsHandledAndLocallyDeleted.size()/2);
        for ( Map.Entry<Long,Boolean> entry : FullTextsController.assignmentsNumsHandledAndLocallyDeleted.entrySet() )
        {
            if ( entry.getValue().equals(true) )
                handledAssignmentsCounts.add(entry.getKey());
        }
        return ResponseEntity.ok(handledAssignmentsCounts);
    }

}
