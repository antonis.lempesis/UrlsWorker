package eu.openaire.urls_worker.components;

import eu.openaire.urls_worker.UrlsWorkerApplication;
import eu.openaire.urls_worker.controllers.FullTextsController;
import eu.openaire.urls_worker.plugins.PublicationsRetrieverPlugin;
import eu.openaire.urls_worker.util.AssignmentsHandler;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;


@Component
public class ScheduledTasks {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    //@Scheduled(fixedRate = 600_000)   // Every 10 mins: 600_000
    public void reportCurrentTime() {
        logger.info("Server is live! Time is now {}", dateFormat.format(new Date()));
    }

    @Scheduled(fixedRate = 900_000)   // Every 15 mins: 900_000
    public void handleNewAssignments() {
        if ( AssignmentsHandler.isAvailableForWork )
            AssignmentsHandler.handleAssignments();
        else {
            //logger.debug("The worker is not available for work at the moment..");   // JUST FOR DEBUG!
        }
    }


    @Scheduled(fixedRate = 43_200_000)   // Every 12 hours.
    public static void deleteHandledAssignmentsFullTexts()
    {
        Set<Map.Entry<Long, Boolean>> entrySet = FullTextsController.assignmentsNumsHandledAndLocallyDeleted.entrySet();
        if ( entrySet.isEmpty() )
            return;

        logger.info("Going to delete the locally stored fullTexts.");

        for ( Map.Entry<Long,Boolean> entry : entrySet )
        {
            if ( entry.getValue().equals(true) )    // It is already deleted, move on.
                continue;

            Long curAssignments = entry.getKey();
            String currentAssignmentsBasePath = PublicationsRetrieverPlugin.assignmentsBasePath + "assignments_" + curAssignments + "_fullTexts" + File.separator;
            logger.debug("Going to delete the files from assignments: " + currentAssignmentsBasePath);

            File curDir = new File(currentAssignmentsBasePath);
            if ( !curDir.isDirectory() ) {
                logger.error("This assignments-dir does not exist: " + currentAssignmentsBasePath);
                continue;
            }

            try {
                FileUtils.deleteDirectory(curDir);
                FullTextsController.assignmentsNumsHandledAndLocallyDeleted.put(curAssignments, true);  // Set the is-handled to true.
            } catch (IOException e) {
                logger.error("The following directory could not be deleted: " + currentAssignmentsBasePath, e);
            }
        }
    }


    //@Scheduled(fixedRate = 20_000)   // Every 20 secs.
    public void testUrlConnection() {
        String urlToCheck = "https://zenodo.org/record/1145726";
        PublicationsRetrieverPlugin.connectWithUrlTest(urlToCheck);
    }

}
