cd "${0%/*}" || (echo "Could not chdir to this script's dir!" && exit)  # Change the working directory to the script's directory, when running from other location.

justInstall=0

if [[ $# -eq 1 ]]; then
	justInstall=$1
elif [[ $# -gt 1 ]]; then
	echo -e "Wrong number of arguments given: ${#}\nPlease execute it like: script.sh <justInstall: 0 | 1>";	exit 1
fi

# Check of the "inputData.txt" file exist, if not, ask to fill it.
inputDataFile="inputData.txt"

if [[ ! -f $inputDataFile ]]; then
  echo -e "The file \"$inputDataFile\" does not exist. Going to create it..\n"

  echo "Give the ID of this worker:"
  read -r workerId

  echo -e "\nGive the max-assignments-limit-per-batch for the Worker to handle: "
  read -r maxAssignmentsLimitPerBatch

  echo -e "\nGive the max-assignments-batches to handle before restart: "
  read -r maxAssignmentsBatchesToHandleBeforeRestart

  echo -e "\nGive the baseUrl of the controller (e.g.: http://IP:PORT/api/):"
  read -r controllerBaseUrl

  touch $inputDataFile
  echo "$workerId,$maxAssignmentsLimitPerBatch,$maxAssignmentsBatchesToHandleBeforeRestart,$controllerBaseUrl" >> $inputDataFile
  echo -e "\n\n"
fi

gradleVersion="7.3.3"

if [[ justInstall -eq 0 ]]; then

  if [ ! -d libs ]; then
    mkdir libs || (echo -e "The directory \"libs\" could not be created! Exiting.." && exit 2)
  fi

  cd libs || exit 3
  git clone https://github.com/LSmyrnaios/PublicationsRetriever.git # We assume there is no previously source-code here, if so, it will be overwritten.

  # Keep a backup of the existing JAR file.
  mv ./publications_retriever-1.0-SNAPSHOT.jar ./publications_retriever-1.0-SNAPSHOT_BACKUP.jar

  cd PublicationsRetriever && sudo apt install -y maven && mvn clean install

  # Copy the created JAR file to the top libs directory.
  cp target/publications_retriever-1.0-SNAPSHOT.jar ../publications_retriever-1.0-SNAPSHOT.jar

  # Delete the directory with the source-code.
  cd ../ && rm -rf PublicationsRetriever

  # Clean, (re)build and run the project.
  cd ../

  if [[ ! -d /opt/gradle/gradle-${gradleVersion} ]]; then
    wget https://services.gradle.org/distributions/gradle-${gradleVersion}-bin.zip
    echo -e "\nAsking for sudo, in order to install 'gradle'..\n"
    sudo mkdir /opt/gradle
    sudo apt install -y unzip && sudo unzip -d /opt/gradle gradle-${gradleVersion}-bin.zip
    #ls /opt/gradle/gradle-${gradleVersion}  # For debugging installation
  fi

  export PATH=/opt/gradle/gradle-${gradleVersion}/bin:$PATH

  # Update the max-heap-size based on the machine's physical memory.
  machine_memory_mb=$(grep MemTotal /proc/meminfo | awk '{print $2}' | xargs -I {} echo "scale=4; {}/1024" | bc)  # It returns the size in MB.
  max_heap_size_mb=$(echo "($machine_memory_mb - 896)/1" | bc) # Leave 896 MB to the system (the "()/1" is used to take the floor value).
  # Now, we replace the "-Xmx" parameter inside the "./build.gradle" file, with "-Xmx${max_heap_size}m"
  echo -e "\n\nThe max-heap-size (-Xmx) will be set to: ${max_heap_size_mb}m\n\n"
  sed -i "s/'-Xmx[0-9]\+[gm]'/'-Xmx${max_heap_size_mb}m'/g" ./build.gradle

  gradle wrapper --gradle-version=${gradleVersion} --distribution-type=bin

  #gradle tasks  # For debugging installation
  #gradle -v # For debugging installation

  gradle clean
  gradle build
else
  export PATH=/opt/gradle/gradle-${gradleVersion}/bin:$PATH # Make sure the gradle is still accessible (it usually isn't without the "export").
fi

gradle bootRun
